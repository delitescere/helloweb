name := "helloweb"

version := "0.2"

scalaVersion := "2.9.2"

scalacOptions ++= Seq()

resolvers ++= Seq(
  "Twittr Repository" at          "http://maven.twttr.com/"
)

libraryDependencies ++= Seq(
  "com.twitter"                 % "finagle-core"          % "6.6.2",
  "com.twitter"                 % "finagle-http"          % "6.6.2"
)

seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)
