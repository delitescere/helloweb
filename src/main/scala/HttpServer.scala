package http

import com.twitter.finagle.{Service, SimpleFilter}
import org.jboss.netty.handler.codec.http._
import org.jboss.netty.handler.codec.http.HttpResponseStatus._
import org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1
import org.jboss.netty.buffer.ChannelBuffers.copiedBuffer
import org.jboss.netty.util.CharsetUtil.UTF_8
import com.twitter.util.Future
import java.net.InetSocketAddress
import java.net.InetAddress
import java.lang.management.ManagementFactory
import com.twitter.finagle.builder.{Server, ServerBuilder}
import com.twitter.finagle.http.Http

/**
 * This example demonstrates a sophisticated HTTP server that handles exceptions
 * and performs authorization via a shared secret. The exception handling and
 * authorization code are written as Filters, thus isolating these aspects from
 * the main service (here called "Respond") for better code organization.
 */
object HttpServer {
  /**
   * A simple Filter that catches exceptions and converts them to appropriate
   * HTTP responses.
   */
  class HandleExceptions extends SimpleFilter[HttpRequest, HttpResponse] {
    def apply(request: HttpRequest, service: Service[HttpRequest, HttpResponse]) = {

      // `handle` asynchronously handles exceptions.
      service(request) handle { case error =>
        val statusCode = error match {
          case _: IllegalArgumentException =>
            FORBIDDEN
          case _ =>
            INTERNAL_SERVER_ERROR
        }
        val errorResponse = new DefaultHttpResponse(HTTP_1_1, statusCode)
        errorResponse.setContent(copiedBuffer(error.getStackTraceString, UTF_8))

        errorResponse
      }
    }
  }

  /**
   * The service itself
   */
  class Respond(private val hostname:String) extends Service[HttpRequest, HttpResponse] {
    def apply(request: HttpRequest) = {
      val response = new DefaultHttpResponse(HTTP_1_1, OK)
      val load = ManagementFactory.getOperatingSystemMXBean.getSystemLoadAverage
      val message = "%s %s %s %s\n".format(hostname, count.incrementAndGet, System.nanoTime, load)
      response.setHeader("Content-Type", "text/plain")
      response.setContent(copiedBuffer(message, UTF_8))
      Future.value(response)
    }

    val count = new java.util.concurrent.atomic.AtomicLong
  }

  def main(args: Array[String]) {
    val handleExceptions = new HandleExceptions
    val hostname = InetAddress.getLocalHost.getHostName
    val respond = new Respond(hostname)
    val logger = java.util.logging.Logger.getLogger("HelloWeb")

    // compose the Filters and Service together:
    val myService: Service[HttpRequest, HttpResponse] = handleExceptions andThen respond

    val port = {
      try {
        sys.props.get("port").map(Integer.parseInt(_)).getOrElse(8080)
      } catch {
        case _:Exception => 8080
      }
    }

    logger.info("Listening on port %s".format(port))

    val server: Server = ServerBuilder()
      .codec(Http())
      .bindTo(new InetSocketAddress(port))
      .name("httpserver")
      .logger(logger)
      .build(myService)
  }
}
