# helloweb

A fast Finagle service that responds in plain text with the host name, request count, system nano time, system load.

Very useful for testing load balancing reverse proxies / ALGs / L7 switches / traffic manager configurations.

## Usage
```
java [-Dport=<port>] -jar helloweb.jar
```
where `<port>` is an IP port number, default `8080`
